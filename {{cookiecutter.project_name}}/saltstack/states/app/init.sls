install_app:
  cmd.run:
    - name: >
        source /home/vagrant/dpe/sampleprojectdpewebapp/miniconda3/bin/activate;
        source activate sampleprojectdpewebapp;
        source env.sh;
        pip install -e .;
    - cwd: /vagrant
    - runas: vagrant
