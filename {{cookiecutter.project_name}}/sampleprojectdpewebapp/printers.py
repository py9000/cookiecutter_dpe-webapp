from contracts import contract
from texttable import Texttable

from sampleprojectdpewebapp.settings import VERSION as versionnumber, DB_URL, TEXTTABLE_STYLE


@contract(returns=None)
def print_testfunc():
    print(render_pdf_html())

    # print(random_tag())
    # t = Texttable()
    # t.set_chars(["-", "|", "+", "-"])
    # t.add_rows([["Name", "Age"], ["Alice", 24], ["Bob", 19]])
    # print(t.draw())
    # print(call_local_shell("df -h"))
    # for line in call_ssh_generator("vagrant",
    #                                "127.0.0.1",
    #                                "sudo apt update"):
    #    print(line)


@contract(returns=None)
def print_version():
    print(versionnumber)


@contract(returns=None)
def print_kv_pairs(dic):
    assert isinstance(dic, dict) is True
    for key in dic.keys():
        print(f"{key} =", dic[key])
